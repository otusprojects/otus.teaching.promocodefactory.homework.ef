﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromoCodeFactoryContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public PromoCodeFactoryContext(DbContextOptions<PromoCodeFactoryContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PromoCode>()
                .HasOne(c => c.Preference);

            modelBuilder.Entity<PromoCode>()
                .Property(p => p.Code).HasMaxLength(10);

            modelBuilder.Entity<Employee>()
                .HasOne(c => c.Role);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(s => s.Customers)
                .UsingEntity<CustomerPreference>(
                    j => j.HasOne(t => t.Preference).WithMany(p => p.CustomerPreferences),
                    j => j.HasOne(t => t.Customer).WithMany(p => p.CustomerPreferences));

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(pt => pt.Customer)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(pt => pt.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(pt => pt.Preference)
                .WithMany(t => t.CustomerPreferences)
                .HasForeignKey(pt => pt.PreferenceId);

            var roles = FakeDataFactory.Roles;
            var employees = FakeDataFactory.Employees;
            var customers = FakeDataFactory.Customers;
            var preferences = FakeDataFactory.Preferences;
            var promoCodes = FakeDataFactory.PromoCodes;

            modelBuilder.Entity<Role>().HasData(roles);
            modelBuilder.Entity<Employee>(e =>
            {
                foreach (var employee in employees)
                {
                    e.HasData(new
                    {
                        Id = employee.Id,
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        Email = employee.Email,
                        AppliedPromocodesCount = employee.AppliedPromocodesCount,
                        RoleId = employee.Role.Id
                    });
                }
            });

            var customerPreferences = customers.SelectMany(c =>
                c.Preferences.Select(p => new
                {
                    CustomerId = c.Id,
                    PreferenceId = p.Id
                })
            );

            modelBuilder.Entity<Customer>(с =>
            {
                foreach (var сustomer in customers)
                {
                    с.HasData(new
                    {
                        Id = сustomer.Id,
                        FirstName = сustomer.FirstName,
                        LastName = сustomer.LastName,
                        FullName = сustomer.FullName,
                        Email = сustomer.Email
                    });
                }
            });

            modelBuilder.Entity<Preference>().HasData(preferences);

            modelBuilder.Entity<CustomerPreference>(с =>
            {
                foreach (var customerPreference in customerPreferences)
                {
                    с.HasData(new
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = customerPreference.CustomerId,
                        PreferenceId = customerPreference.PreferenceId
                    });
                }
            });

            
            modelBuilder.Entity<PromoCode>(p =>
            {
                foreach (var promoCode in promoCodes)
                {
                    p.HasData(new
                    {
                        Id = promoCode.Id,
                        Code = promoCode.Code,
                        ServiceInfo = promoCode.ServiceInfo,
                        BeginDate = promoCode.BeginDate,
                        EndDate = promoCode.EndDate,
                        PartnerName = promoCode.PartnerName,
                        PartnerManagerId = promoCode.PartnerManager.Id,
                        PreferenceId = promoCode.Preference.Id,
                        CustomerId = customers.FirstOrDefault()?.Id
                    });
                }
            });
        }
    }
}
