﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepository : EfRepository<PromoCode>
    {
        public PromoCodeRepository(DbContext context) : base(context)
        {
        }

        public async Task AddPromoCodeToCustomer(PromoCode promoCode, string preference)
        {
            var customers = await _context.Set<Customer>()
                .Where(c => c.Preferences.Any(p => p.Name.Equals(preference)))
                .ToListAsync();

            var promoCodes = new List<PromoCode>();
            customers.ForEach(c =>
            {
                promoCode.Customer = c;
                promoCodes.Add(promoCode);
            });

            await base.CreateRangeAsync(promoCodes);
        }
    }
}
