﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>
    {
        public CustomerRepository(DbContext context) : base(context)
        {
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await _context.Set<Customer>()
                .Include(c => c.PromoCodes)
                .Include(c => c.Preferences)
                .FirstOrDefaultAsync(c => c.Id == id);
        }  

        public override async Task<Customer> CreateAsync(Customer customer)
        {
            if (await _context.Set<Customer>()
                    .AnyAsync(c => c.Email == customer.Email))
            {
                return await Task.FromException<Customer>(new ArgumentException());
            }

            var preferences = await _context.Set<Preference>()
                .Where(p => customer.Preferences.Contains(p))
                .ToListAsync();
            customer.Preferences = preferences;
            return await base.CreateAsync(customer);
        }

        public override async Task<Customer> EditAsync(Customer customer)
        {
            if (!await _context.Set<Customer>().AnyAsync(t => t.Id == customer.Id))
            {
                return await Task.FromException<Customer>(new ArgumentOutOfRangeException());
            }

            var customerPreference = _context.Set<CustomerPreference>().FirstOrDefault(cp => cp.CustomerId == customer.Id);
            if (customerPreference != null)
            {
                _context.Set<CustomerPreference>().Remove(customerPreference);
            }

            return await base.EditAsync(customer);
        }

        public override async Task<Customer> DeleteAsync(Customer customer)
        {
            if (!await _context.Set<Customer>().AnyAsync(t => t.Id == customer.Id))
            {
                return await Task.FromException<Customer>(new ArgumentOutOfRangeException());
            }

            var promoCodes = await _context.Set<PromoCode>()
                .Where(p => p.CustomerId == customer.Id)
                .ToListAsync();
            if (promoCodes.Any())
            {
                _context.Set<PromoCode>().RemoveRange(promoCodes);
            }

            return await base.DeleteAsync(customer);
        }
    }
}
