﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string FullName => $"{FirstName} {LastName} {MiddleName}";

        public string Email { get; set; }

        public IEnumerable<Preference> Preferences { get; set; }

        public IEnumerable<PromoCode> PromoCodes { get; set; }

        public IEnumerable<CustomerPreference> CustomerPreferences { get; set; }

    }
}