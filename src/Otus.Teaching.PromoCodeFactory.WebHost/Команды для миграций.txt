Add-Migration InitMigration -StartupProject Otus.Teaching.PromoCodeFactory.WebHost
Update-Database -StartupProject Otus.Teaching.PromoCodeFactory.WebHost
Add-Migration SecondMigration -StartupProject Otus.Teaching.PromoCodeFactory.WebHost
Update-Database -StartupProject Otus.Teaching.PromoCodeFactory.WebHost