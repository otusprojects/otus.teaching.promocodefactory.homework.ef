﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _repository;

        public PromoCodesController(IRepository<PromoCode> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получить все промокоды.
        /// </summary>
        /// <returns>Список промокодов.</returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _repository.GetAllAsync();
            var promoCodeShortResponse = promoCodes.Select(p => new PromoCodeShortResponse()
            {
                Id = p.Id,
                Code = p.Code,
                BeginDate = p.BeginDate.ToString(CultureInfo.InvariantCulture),
                EndDate = p.EndDate.ToString(CultureInfo.InvariantCulture),
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            });
            return Ok(promoCodeShortResponse);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promoCode = new PromoCode
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo
            };
            
            await (_repository as PromoCodeRepository)?.AddPromoCodeToCustomer(promoCode, request.Preference);
            return Ok();
        }
    }
}